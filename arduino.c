#include <Servo.h>

Servo servo_ear_l;
Servo servo_ear_r;
Servo servo_arm_l;
Servo servo_arm_r;

void setup()
{
  servo_ear_l.attach(1);
  servo_ear_r.attach(2);
  servo_arm_l.attach(3);
  servo_arm_r.attach(4);
}

void loop()
{
  // receiving data from SPI

  // ------------ RIGHT ARM --------------
  if (data == 0x00){
    servo_arm_r.write(180)
  }
  if (data == 0x01){
    servo_arm_r.write(90)
  }
  if (data == 0x02){
    servo_arm_r.write(0)
  }

  // ------------ LEFT ARM --------------
  if (data == 0x03){
    servo_arm_l.write(180)
  }
  if (data == 0x04){
    servo_arm_l.write(90)
  }
  if (data == 0x05){
    servo_arm_l.write(0)
  }

  // ------------ RIGHT EAR --------------
  if (data == 0x10){
    servo_ear_r.write(180)
  }
  if (data == 0x11){
    servo_ear_r.write(90)
  }
  if (data == 0x12){
    servo_ear_r.write(0)
  }

  // ------------ LEFT EAR --------------
  if (data == 0x13){
    servo_ear_l.write(180)
  }
  if (data == 0x14){
    servo_ear_l.write(90)
  }
  if (data == 0x15){
    servo_ear_l.write(0)
  }

  delay(200);
}
