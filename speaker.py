import pygame


class Speaker:
    def __init__(self):
        self.device = pygame.mixer
        self.device.init()

    def play(self, file):
        self.device.music.load(file)
        self.device.music.play()
        # note -1 for playing in loops
        # while self.device.music.get_busy():
        #     continue

    def is_playing(self):
        if self.device.music.get_busy():
            return True
        else:
            return False
